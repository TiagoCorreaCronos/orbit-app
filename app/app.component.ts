import { Component }          from '@angular/core';
import { ROUTER_DIRECTIVES }  from '@angular/router';

import { OrbitService }        from './orbit.service';

@Component({
  selector: 'my-app',

  template: `
    <h1>{{title}}</h1>
    <nav>
    <!--Here is where each of the pages to routed comes in
        In our definition when there is no route, it points to login
    -->
    </nav>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['app/app.component.css'],
  directives: [ROUTER_DIRECTIVES],
  providers: [
    OrbitService,
  ]
})
export class AppComponent {
  title = 'Project Orbit';
}
