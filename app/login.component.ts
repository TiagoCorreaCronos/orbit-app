import { Component, OnInit, Input } from '@angular/core';
import { Router }           from '@angular/router';

import { User }        from './user';
import { OrbitService } from './orbit.service';

@Component({
  selector: 'my-dashboard',
  templateUrl: 'app/login.component.html',
  styleUrls: ['app/login.component.css']
})

export class LoginComponent implements OnInit {
  user: User;

  constructor(
    private router: Router,
    private orbitService: OrbitService) {
  }

  ngOnInit() {
    this.user = new User();
  }

  gotoDetail(user: User) {
    this.orbitService.validateUser(user)
         .then(user => this.user = user);

    console.log('The result was:');
    console.log(user);
  }
}
