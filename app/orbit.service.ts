import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { User } from './user';

@Injectable()
export class OrbitService {

  private APIUrl = 'http://0.0.0.0:8000/json/connect.json';  // URL to web api

  constructor(private http: Http) { }

  validateUser(u: User): Promise<User> {
    console.log('Consulting http://0.0.0.0:8000/json/connect.json with the following parameters:' + u.username + ' and ' + u.password);
    return this.http.get(this.APIUrl)
               .toPromise()
               .catch(this.handleError);
  }
}
